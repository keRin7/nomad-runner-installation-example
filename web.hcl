job "web1" {
  datacenters = ["dc1"]
  group "web1" {
    count = 2
    task "web1" {
      driver = "docker"
      config {
        image = "hashicorp/http-echo:latest"
        args  = [
          "-listen", ":8080",
          "-text", "Test",
        ]
        port_map {
          http = 8080
        }
      }
      resources {
        network {
          mbits = 100
          port "http" { }
        }
      }
      service {
        name = "web1"
        port = "http"     
        tags = [
          "traefik.tags=web1",
          "traefik.frontend.rule=PathPrefixStrip:/web1",
        ]

      }
    }
  }
}
